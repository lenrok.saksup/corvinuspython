import re
import datetime

def  get_dt_from_logline(line):
	matches = re.search(r"^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})",line)
	year = int(matches.group(1))
	month = int(matches.group(2))
	day = int(matches.group(3))
	hour = int(matches.group(4))
	minute = int(matches.group(5))
	second = int(matches.group(6))
	dt = datetime.datetime(year,month,day,hour,minute,second)
	return dt

def  parse_connect_line(line):
		matches = re.search(r"\]\[(\w+);([^;]+);(\w+)\]",line)
		return matches.groups()

#teszt
if  __name__ == "__main__":
	print("Teszt a get_dt_from_logline függvényhez:")
	line1 = "2020-03-22 01:10:47.707 [Information] [000453][6dccd7e853ae750d039d920801bd5da9;adatb/gyak01/;2] CLASSROOM: #6dccd7e853ae750d039d920801bd5da9just joined classroom (adatb/gyak01/) -> 2 user(s) online"
	line2 = "2020-03-22 01:13:13.465 [Information] [000411][6dccd7e853ae750d039d920801bd5da9;adatb/gyak01/;2] Disconnect: user #6dccd7e853ae750d039d920801bd5da9left classroom (adatb/gyak01/) -> 1 user(s) online"

	s1 = get_dt_from_logline(line1)
	s2 = get_dt_from_logline(line2)
	print(s2-s1)

class  Session:
	closed = False
	seconds = 0
	def  __init__(self, user, document, open_at):
		self.open_at = open_at
		self.user = user
		self.document = document
	def  close(self, closed_at):
		self.closed_at = closed_at
		self.closed = True
		self.seconds = (closed_at - self.open_at).seconds