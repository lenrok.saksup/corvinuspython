import os
import re

path = "log_data/"
code_set = set() # Üres halmaz, ide kerülenk majd az eredmények
line_count = 1

def process_file(filename):
    global line_count
    file_in = open(filename,"rt", encoding="utf-8")    
    for line in file_in.readlines():
        matches = re.findall("\[([^\]]*)\]",line)
        line_count += 1
        #print(line)
        #print(matches)
    
        if  len(matches)>=2 and (matches[1] not in code_set): # Van olyan sor, amiben nincs szögletes zárójel. Ezen leállna a futás. 
            code_set.add(matches[1])
            print(line)
    file_in.close()
    
files = os.listdir(path)
for filename in files:
    process_file(path + filename)

print(code_set)
print(line_count)