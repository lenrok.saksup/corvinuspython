import os
import re
import matplotlib.pyplot as plt
import log_helper

path = "log_data/"

students = dict() # itt tároljuk majd hallgatónként a session-öekt

def  process_file(filename):
	global students
	file_in = open(filename,"rt", encoding="utf-8")
	for line in file_in.readlines():
		matches = re.findall("\[([^\]]*)\]",line)                   # megkeressük a bejegyzéstípust
		if  len(matches)>=2  and (matches[1] == '000453'):          # Belépés típus
			user, document, x = log_helper.parse_connect_line(line) # kinyerjük a felhasználó és a dukumentum nevét, x, az aktív kapcsolatok száma nem kell
			date = log_helper.get_dt_from_logline(line)             # kinyerjük az időpontot
			session = log_helper.Session(user,document,date)        # új Session-t hozunk létre
			if user not  in students.keys():                        # ha még nem volt ez a diák, csinálunk neki listát a szótárben
				students[user]=list()
			students[user].append(session)                          # a diák listájáb betesszük a session-t
			
		if  len(matches)>=2  and (matches[1] == '000411'): #Kilépés típus
			user, document, x = log_helper.parse_connect_line(line)
			date = log_helper.get_dt_from_logline(line)
			# megkeressük a megfelelő session-öket
			sessions_for_user = [x for x in students[user] if x.document == document and x.closed == False]
			if  len(sessions_for_user) >= 1:
				sessions_for_user[-1].close(date)
			else:
				print("Close without a open")
			if  len(sessions_for_user)>1:
				print("Open without a close")
		if  len(matches)>=2  and (matches[1] == '000110'): #Server restart
			date = log_helper.get_dt_from_logline(line)
			for user, session in students.items():
				for x in session:
					if  not x.closed:		
						x.close(date)

	file_in.close()

# Fájlok feldolgozása

files = os.listdir(path)
for filename in files:
	process_file(path + filename)
  
# Összesítés

times = []
for kulcs in students:
	t = sum(s.seconds for s in students[kulcs]) / 3600
	times.append(t)
	print(kulcs,t)

# Kirajzolás

plt.hist(times, density=False, bins=30) # `density=False` would make counts
plt.ylabel('count')
plt.xlabel('hours')
plt.show()